package step_definitions;

import io.cucumber.java8.En;
import stockbit.test.page_object.LoginPage;

public class LoginSteps implements En {
    LoginPage loginPage = new LoginPage();

    public LoginSteps() {
        Given("^User click entry point login$", () -> {
                loginPage.ClickLoginEntryPoint();
        });
        And("User input username {string}", (String username) ->
                loginPage.InputUsername(username));
        When("^User input password \"([^\"]*)\"$", (String password) ->
                loginPage.InputPassword(password));
        And("^User click login button$", () ->
                loginPage.ClickButtonLogin());
        And("^User click Lewati smart login dialog$", () -> {
            loginPage.clickButtonLewatiSmartLogin();
        });
        And("^User click Lewati avatar dialog$", () -> {
            loginPage.clickButtonLewatiAvatar();
        });
        Then("^User see Watchlist page$", () -> {
            loginPage.seeWatchlistPage();
        });
        Then("^User see invalid message$", () -> {
            loginPage.seePassSalah();
        });
        Then("^User see message invalid blank input$", () -> {
            loginPage.seeUnamePassKosong();
        });
        Then("^User see alert \"([^\"]*)\" in login page$", (String alert) -> {
            loginPage.checkAlert(alert);
        });
    }
}
