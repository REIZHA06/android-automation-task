Feature: Login feature

  Scenario: Login with invalid password
    Given User click entry point login
    When User input username "reza"
    And User input password "ganteng"
    And User click login button
    Then User see alert "Username atau password salah. Mohon coba lagi." in login page

  Scenario: Login with invalid username
    Given User click entry point login
    When User input username "reza"
    And User input password "ganteng"
    And User click login button
    Then User see alert "Username atau password salah. Mohon coba lagi." in login page

  Scenario: Success login
    Given User click entry point login
    When User input username "trimegaw"
    And User input password "2024siapmaju"
    And User click login button
    And User click Lewati smart login dialog
    And User click Lewati avatar dialog
    Then User see Watchlist page

  Scenario: Login with blank username and password
    Given User click entry point login
    When User click login button
    Then User see message invalid blank input