package stockbit.test.page_object;

import org.openqa.selenium.By;
import stockbit.test.base.BasePageObject;

import java.util.Objects;

public class LoginPage extends BasePageObject {

    public void ClickLoginEntryPoint(){
        tap("ENTRY_POINT_LOGIN");
    }

    public void InputUsername(String username){
        InputText("FIELD_USERNAME", username);
    }

    public void InputPassword(String password){
        InputText("FIELD_PASSWORD", password);
    }

    public void ClickButtonLogin(){
        tap("BUTTON_LOGIN");
    }

    public void checkAlert(String alert){
        isTextSame("ALERT_LOGIN", alert);
    }

    public void inputUsername(String username) {
        InputText("FIELD_USERNAME", username);
    }

    public void inputPassword(String password) {
        InputText("FIELD_PASSWORD", password);
    }

    public void clickButtonLewatiSmartLogin(){
        tap("BUTTON_SMART_LOGIN_SKIP");
    }

    public void clickButtonLewatiAvatar(){
        tap("BUTTON_CHOOSE_AVATAR_SKIP");
    }

    public void seeWatchlistPage(){
        IsDisplayed("WATCHLIST");
    }

    public void seePassSalah(){
        IsDisplayed("ALERT_PASSWORD_SALAH");
    }

    public void seeUnamePassKosong(){
        IsDisplayed("ALERT_USERNAME_PASSWORD_KOSONG");
    }


}
