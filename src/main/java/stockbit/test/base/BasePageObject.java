package stockbit.test.base;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.github.cdimascio.dotenv.Dotenv;
import org.openqa.selenium.*;
import stockbit.test.android_driver.AndroidDriverInstance;
import stockbit.test.utils.Utils;

import java.util.List;
import java.util.Objects;

public class BasePageObject {

    Dotenv env = Dotenv.load();

    public AndroidDriver getDriver(){
        return AndroidDriverInstance.androidDriver;
    }

    public By element(String elementLocator){
        //Utils.loadElementProperties("\Users\Reizha Nurul Huda\IdeaProjects\android-automation-task\src\test\resources\elementsLoginPage.properties");
        String elementValue = Utils.ELEMENTS.getProperty(elementLocator);
        if (elementValue == null) {
            throw new NoSuchElementException("Couldn't find element : " + elementLocator);
        } else {
            String[] locator = elementValue.split("_");
            String locatorType = locator[0];
            String locatorValue = elementValue.substring(elementValue.indexOf("_") + 1);
            switch (locatorType){
                case "id":
                    if(Objects.equals(env.get("ENV"), "prod")){
                        return By.id("com.stockbit.android:id/" + locatorValue);
                    }else{
                        return By.id("com.stockbitdev.android:id/" + locatorValue);
                    }
                case "xpath":
                    return By.xpath(locatorValue);
                default:
                    throw new IllegalStateException("Unexpected value: " +locatorType);
            }
            /*switch (locatorType.hashCode()) {
                case 3355:
                    if (locatorType.contains("id")) {
                        var = 1;
                    }
                    break;
                case 3373707:
                    if (locatorType.contains("name")) {
                        var = 2;
                    }
                    break;
                case 114256029:
                    if (locatorType.contains("xpath")) {
                        var = 3;
                    }
                    break;
                case -2141255700:
                    if (locatorType.contains("containsText")) {
                        var = 4;
                    }
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + var);
            }

            switch (var) {
                case 1:
                    return By.id(locatorValue);
                case 2:
                    return MobileBy.name(locatorValue);
                case 3:
                    return MobileBy.xpath(locatorValue);
                case 4:
                    return MobileBy.xpath("//*[contains(@name, '" + locatorValue + "')]");
                default:
                    return null;
            }*/
        }
    }

    public Integer getExplicitTimeout() {
        return 15;
    }

    public void InputText(String locator, String text){
        getDriver().findElement(element(locator)).sendKeys(text);
    }

    public void tap(String locator){
        getDriver().findElement(element(locator)).click();
    }

    public void IsDisplayed(String locator){
        getDriver().findElement(element(locator)).isDisplayed(); }

    public void printError(String text){
        throw new AssertionError(text);
    }

    public  void isTextSame(String locator, String text){
        String getText = getText(By.id(locator));
        if (!getText.equals(text)) {
            printError(String.format("Expected string [%s] not match with actual string [%s]", text, getText));
        }
    }

    public String getText(By element){
          return getDriver().findElement(element).getText();
    }


}
